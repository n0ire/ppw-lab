from django.urls import re_path, path
from .views import index, register, education, resume, project, profiles

#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^register/', register, name='register'),
    re_path(r'^education/', education, name='education'),
    re_path(r'^resume/', resume, name='resume'),
    re_path(r'^project/', project, name='project'),
    re_path(r'^list_profile/', profiles, name='list_profile'),
    # re_path(r'^jadwal/', jadwal, name='jadwal'),
]

