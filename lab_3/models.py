from django.db import models

# Create your models here.
class Profile(models.Model):
    username = models.CharField(max_length=200)
    fullname = models.CharField(max_length=200)
    # birth_date = models.DateField(null=True)
    email = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

# class JadwalPribadi(models.Model):
#     hari = models.CharField(max_length=20)
#     tanggal = models.IntegerField()
#     jam = models.IntegerField()
#     nama_kegiatan = models.CharField(max_length=200)
#     tempat = models.CharField(max_length=200)
#     kategori = models.CharField(max_length=200)
