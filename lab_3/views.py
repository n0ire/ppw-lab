from django.shortcuts import render
from datetime import datetime, date
from .models import Profile
from .forms import ProfileForm

# Create your views here.
def index(request):
    response = {}
    return render(request, 'index_lab3.html', response)

def resume(request):
    response = {}
    return render(request, 'resume.html', response)

def education(request):
    response = {}
    return render(request, 'education.html', response)

def project(request):
    response = {}
    return render(request, 'project.html', response)


def register(request):
    form = ProfileForm(request.POST or None)
    if request.method == 'POST':
        if (form.is_valid()):
            username = request.POST.get("username")
            fullname = request.POST.get("fullname")
            email = request.POST.get("email")
            password = request.POST.get("password")
            profile = Profile(fullname=fullname, username=username, email=email, password=password)
            print(profile)
            response = {
                "form" : ProfileForm,
                "message" : "User Created",
                "color" : "aqua",
            }
            return render(request, 'register.html', response)
        else:
            print(form) 
            response = {
                "form" : ProfileForm,
                "message" : "Form is not Valid",
                "color" : "red",
            }
            return render(request, 'register.html', response)

    # IF GET REQUEST
    response = {
        "message" : "",
        "form" : ProfileForm,
    }
    return render(request, 'register.html', response)

def profiles(request):
    profiles = Profile.objects.all()
    response = {
        "profiles" : profiles,
    }
    return render (request, 'profile.html', response)

def jadwal(request):
    if(request.POST):
        pass

    response = {

    }
    return render(request, 'jadwal.html', response)
