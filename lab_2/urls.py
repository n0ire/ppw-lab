from django.urls import path, re_path
from .views import index

#url for app, add your URL Configuration

urlpatterns = [
    re_path(r'^$', index, name='index'),
]
