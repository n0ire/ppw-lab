from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime, date
import json
# Enter your name here
mhs_name = 'Fachry Nataprawira'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 9, 15) #TODO Implement this, format (Year, Month, Date)
npm = 1706039703 # TODO Implement this
# Create your views here.

def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)

def home(request):
    # return HttpResponse("Hello")
    if(request.user.is_authenticated):
            try:
                request.session['firstLogged']
            except:
                request.session['firstLogged'] = True
                request.session['full_name'] = "{} {}".format(request.user.first_name, request.user.last_name)
                request.session['username'] = request.user.username

    for key, val in request.session.items():
        print("""   ============
        [{}]====[{}]
        ============""".format(key, val))

    return render(request, 'home.html', {})

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
