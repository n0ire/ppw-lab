from django.db import models
from django.utils import timezone

# Create your models here.

class Status(models.Model):
    status = models.CharField(max_length=2048, blank=False)
    name = models.CharField(max_length=2048, default='anonymous')
    created_at = models.DateTimeField(default=timezone.now)

