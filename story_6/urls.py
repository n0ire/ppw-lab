from django.contrib import admin
from django.urls import path, include
from .views import landing, profile

urlpatterns = [
    path('', landing, name='index'),
    path('profile/', profile, name='profile'),
]