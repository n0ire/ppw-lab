# Generated by Django 2.1.2 on 2018-10-10 18:42

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('story_6', '0006_auto_20181010_1841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='status',
            name='name',
            field=models.CharField(default='anonymous', max_length=2048),
        ),
        migrations.AlterField(
            model_name='status',
            name='status',
            field=models.CharField(default='nothing todo ...', max_length=2048),
        ),
    ]
