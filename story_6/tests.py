import time
from django.test import TestCase
from .models import Status
from .forms import StatusForm
from .views import *
from django.test import Client
from django.urls import resolve

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium import webdriver




client = Client()

class StatusModelTest(TestCase):
    """
    Status models test
    """
    def test_status_model_instance_created(self):
        """
        Testing if status models instance was created to database
        """
        contoh_text =  "Hello World"
        early_count = Status.objects.count()
        status_test = Status.objects.create(status=contoh_text)
        last_count = Status.objects.count()
        self.assertEqual(early_count + 1, last_count)  
        self.assertEqual(status_test.status, contoh_text)  

class StatusViewTest(TestCase):
    """
    Test views' status code response
    """
    def test_status_view_get_return_200(self):
        """
        Test request get return 200
        """
        response = client.get('/story-6/')
        # coba_post = requests.post('/tdd/', data={"status":"Hello world"})
        Status.objects.create(status="Aku baik saja hari ini")
        self.assertEqual(response.status_code, 200)  
        # self.assertEqual(coba_post.status_code, 200)  

    def test_status_views_using_landing_func(self):
        """
        Test if a reverse using correct views function
        """
        found = resolve('/story-6/')
        self.assertEqual(found.func, landing)

    def test_status_request_post_success(self):
        status = "hay"
        name = "testuser"
        response_post = client.post('/story-6/', {'status': status, 'name': name})
        self.assertEqual(response_post.status_code, 302)

    def test_story6_post_error_and_render_the_result(self):
        response_post = Client().post('/story-6/', {'name': '', 'status': ''})
        self.assertEqual(response_post.status_code, 302)


class StatusFormTest(TestCase):
    def test_form_status_input(self):
        form = StatusForm()
        self.assertIn('name="status"', form.as_p())
        self.assertIn('name="name', form.as_p())

class ProfileViewsTest(TestCase):
    def test_profile_get_status_code_200(self):
        response = client.get('/story-6/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_get_correct_output_html(self):
        response = client.get('/story-6/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn("Muhammad Fachry Nataprawira", html_response)

    def test_status_views_using_landing_func(self):
        found = resolve('/story-6/profile/')
        self.assertEqual(found.func, profile)

class StatusFunctionalTest(LiveServerTestCase):

    def setUp(self):
        op = Options()
        op.add_argument('--dns-prefetch-disable')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=op) 
        self.selenium.implicitly_wait(10)
        super(StatusFunctionalTest, self).setUp()

    def test_input_status(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/story-6/'))
        selenium.find_element_by_id('id_name').send_keys("SeleniumUser")
        selenium.find_element_by_id('id_status').send_keys("Coba Coba")
        selenium.find_element_by_id('submit_status').send_keys(Keys.RETURN)
        page = selenium.page_source
        self.assertIn('SeleniumUser', page)
        self.assertIn('Coba Coba', page)

    def test_layout_using_selenium(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/story-6/'))
        page = selenium.page_source
        self.assertIn('Hello apa kabar?', page)
        self.assertTrue(selenium.title, 'Status')
        self.assertTrue(selenium.find_element_by_css_selector('div.grid-container > h1.font-kayak').is_displayed()) 

    def test_style_using_senelium(self):
        selenium = self.selenium
        selenium.get('%s%s' % (self.live_server_url, '/story-6/'))
        page = selenium.page_source
        self.assertTrue(selenium.find_element_by_class_name('font-kayak').is_displayed()) 
        self.assertTrue(selenium.find_element_by_class_name('grid-container').is_displayed()) 

    def tearDown(self):
        self.selenium.close()
        super(StatusFunctionalTest, self).tearDown()
