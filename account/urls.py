
from django.conf.urls import include
from django.urls import re_path, path
from django.contrib import admin
from .views import login, logout
from django.contrib import auth

urlpatterns = [
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
]
