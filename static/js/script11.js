var counter = 0;
function fav(caller) {
      console.log("clicked")
      if( $(caller).attr('src') == yellow_star) {
          $(caller).attr('src', blue_star);
          counter--;
          $("#favorited").html(counter);
      } else {
          $(caller).attr('src', yellow_star);
          counter++;
          $("#favorited").html(counter);
      }
    }

$(document).ready(function() {
  var urlParams = new URLSearchParams(window.location.search);
  $.get({
    url: get_book_json_url + "?q=" + urlParams.get('q'),
    context: document.body,
    success: function(data) {
      $.each(data['items'], function(i, item) {
        var storedBooks = JSON.parse(localStorage.getItem("storedBook"));
        console.log(storedBooks);
        var imgButton = $.inArray(item['id'], storedBooks) > -1 ? yellow_star : blue_star;
        $("#books-list").append(
          `
          <tr>
            <td class='text bg-grey'>
              <img width='50px' src='${ item["volumeInfo"]["imageLinks"]["smallThumbnail"] }' alt=''>
            </td>
            <td class="text bg-grey">${ item["volumeInfo"]["title"] }</td>
            <td class="text bg-grey">${ item["volumeInfo"]["authors"][0] }</td>
            <td class="text bg-grey">${ item["volumeInfo"]["publishedDate"] }</td>
            <td class="text bg-grey">
              <img class="fav" width="30" src="${imgButton}" alt="" onclick="fav(this)">
            </td>
          </tr>
          `)
      });
    }
  });
});

$('#searchbar').on("keyup", function(e){
    q = e.currentTarget.value
    console.log(q)
    $.get({
      url: get_book_json_url + "?q=" + q,
      context: document.body,
      success: function(data) {
        $("#books-list").html('')
        $.each(data['items'], function(i, item) {
          try{
            var img = (
              item["volumeInfo"]["imageLinks"]["smallThumbnail"]) ?
            `
              <td class='text bg-grey'>
                <img width='50px' src='${ item["volumeInfo"]["imageLinks"]["smallThumbnail"] }' alt=''>
              </td>
            `
            : "";
          } catch(err) {
              var img = ""
          }
          $("#books-list").append(
            `
            <tr>
              ${img}
              <td class="text bg-grey">${ item["volumeInfo"]["title"] }</td>
              <td class="text bg-grey">${ item["volumeInfo"]["authors"] }</td>
              <td class="text bg-grey">${ item["volumeInfo"]["publishedDate"] }</td>
              <td class="text bg-grey">
                <img class="fav" width="30" src= ` + blue_star + ` alt="" onclick="fav(this)">
              </td>
            </tr>
            `)
        });
      }
    });
});
