$("#exampleSwitch").change(function () {
    if(this.checked){
      $('link[href="/static/css/dark.css"]').attr('href', '/static/css/light.css');
    } else {
      $('link[href="/static/css/light.css"]').attr('href', '/static/css/dark.css');
    }
  });
