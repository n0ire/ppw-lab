from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
    hari = models.CharField(max_length=20)
    tanggal = models.CharField(max_length=20)
    jam = models.CharField(max_length=20)
    nama_kegiatan = models.CharField(max_length=200)
    tempat = models.CharField(max_length=200)
    kategori = models.CharField(max_length=200)
