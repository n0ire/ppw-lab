from django.db import models

# Create your models here.
class Favorited(models.Model):
    book_id = models.CharField(primary_key=True, max_length=200)