"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path, path
from django.contrib import admin
from lab_1.views import home

urlpatterns = [
    path('', home, name="index-home"),

    path('account/', include(('account.urls', 'account'), namespace="account")),
    path('auth/', include('social_django.urls', namespace='social')),  # <- Here

    path('admin/', admin.site.urls),

    path('lab-1/', include(('lab_1.urls', 'lab_1'), namespace="lab-1")),
    path('lab-2/', include(('lab_2.urls', 'lab_2'), namespace="lab-2")),

    path('lab-3/', include(('lab_3.urls', 'lab_3'), namespace="lab-3")),
    path('lab-4/', include('lab_3.urls')),

    path('story-6/', include(('story_6.urls', 'story_6'), namespace="story-6")),
    path('story-7/', include('story_6.urls')),

    path('story-8/', include(('story_8.urls', 'story_8'), namespace="story-8")),
    path('story-9/', include(('story_9.urls', 'story_9'), namespace="story-9")),

    path('story-10/', include(('story_10.urls', 'story_10'), namespace="story-10")),

    path('jadwal/', include('jadwal.urls')),


]
