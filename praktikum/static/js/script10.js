$(function() {
  $("#accordion").accordion({
    collapsible: true
  });
  parseSubscriber();
});

var passwordModal = new Foundation.Reveal($('#animatedModal10'));
var isEmailValidGlobal = false;
var timer = null;
$('#emailInput').keydown(function() {
  formComplete();
  $("#emailDiv small").html("");
  clearTimeout(timer);
  timer = setTimeout(checkEmail, 500)
  formComplete()
});

$("#nameInput").on("input", function() {
  $("#emailDiv small").html("");
  formComplete();
  clearTimeout(timer);
  timer = setTimeout(checkEmail, 500)
});

$("#passwordInput").on("input", function() {
  $("#emailDiv small").html("");
  formComplete();
  clearTimeout(timer);
  timer = setTimeout(checkEmail, 500)
});

function checkEmail() {
  $("#emailDiv small").html("");

  data = {
    "inputValue": $("#emailInput").val(),
    "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
  }
  $.ajax({
    type: "POST",
    url: "check-email/",
    data: data,
    dataType: "json"
  })
  .done(isEmailAvailabe)
  .fail(function(res) {
    console.log(res)
  });
}

function isEmailAvailabe(data) {
  $("#emailDiv small").html("");
  console.log(data)
  if (data['status_code'] === 400) {
    $("#emailDiv").append(`<small class='error'>${data['message']}</small>`);
    isEmailValidGlobal = false;
    formComplete();
  } else {
    console.log(data)
    $("#emailDiv").append(`<small class='success'>${data['message']}</small>`);
    isEmailValidGlobal = true;
    formComplete();
  }
}

function formComplete() {
  let isEmailValid = ($("#emailInput").val() !== "") ? true : false;
  let isPasswordValid = ($("#passwordInput").val() !== "") ? true : false;
  let isNameValid = ($("#nameInput").val() !== "") ? true : false;
  let isPassLengthValid = ($("#passwordInput").val().length > 6) ? true : false;

  if ($("#emailInput").val() !== "" && $("#passwordInput").val() !== "" &&
    $("#nameInput").val() !== "" && $("#passwordInput").val().length > 6 && isEmailValidGlobal) {
    $('#submitButton').prop('disabled', false);
  } else {
    $('#submitButton').prop('disabled', true);
  }
}

function subs() {
  data = {
    "name": $("#nameInput").val(),
    "email": $("#emailInput").val(),
    "password": $("#passwordInput").val(),
    "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
  }
  $.ajax({
    type:"POST",
    url: "subscribe/",
    data: data,
    dataType: "json",
  })
    .done(function(data) {
      if (data.status_code === 400) {
        alert(data.message);
        parseSubscriber()
      } else {
        alert(data.message);
        parseSubscriber()
      }
    })
    .fail(function(res) {
      alert(res)
      parseSubscriber()
    })
}

function parseSubscriber() {
  $.ajax({
    type:"GET",
    url: "subscribers/",
    dataType: "json",
  })
    .done(function(data) {
      $("#list-of-subscribers").html('');
      $.each(data, (i, item) => {
        $("#list-of-subscribers").append(
        `
          <div class="small-6">
            <span class="text bg-grey">${ item["fields"]["email"] }</span>
          </div>
          <div class="small-6">
            <button email="${item['fields']['email']}" pk="${item['pk']}" onclick="unsubscribe(this);" class="button default" name="unsub-btn">Unsubscribe</button>
          </div>
        `)
          console.log(i + item['fields']['email']);
      })
    })
    .fail(function(res) {
      console.log("Nope")
    })
}
$('button[name=unsub-btn]').on("click", function() {
  console.log("as")
})

function unsubscribe(caller) {
  var _email = $(caller).attr('email')
  var _pk = $(caller).attr('pk')
  $("#emailConf").html(_email)
  $("#_pk").html(_pk)
  passwordModal.open();
}

function unsubs(caller) {
  var _pass = $("#passwordConf").val();
  var _email = $("#emailConf")[0].textContent;
  var _pk = $("#_pk")[0].textContent;
  var _url = `subscribers/${_pk}/unsubscribe/`;

  console.log(_pk)
  console.log(_email)
  console.log(_pass)

  var data = {
    "pk": _pk,
    "email": _email,
    "password": _pass,
    "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
  }

  console.log(data)

  $.ajax({
    type:"POST",
    url: _url,
    data: data,
    dataType: "json",
  })
    .done(function(data) {
        alert(data.message);
        parseSubscriber();
        passwordModal.close();
    })
    .fail(function(res) {
      alert(res)
      parseSubscriber()
    })
}
